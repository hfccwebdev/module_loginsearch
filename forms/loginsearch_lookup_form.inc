<?php

/**
 * @file
 * This file contains page callbacks for the loginsearch module.
 *
 * @see loginsearch.module
 */

/**
 * Define the Login Search form.
 *
 * @ingroup forms
 */
function loginsearch_lookup_form($form, &$form_state) {
  global $user;

  $form['name'] = [
    '#type' => 'textfield',
    '#title' => t('Last Name'),
    '#size' => 30,
    '#maxlength' => 64,
    '#required' => TRUE,
    '#default_value' => !empty($form_state['values']['name']) ? $form_state['values']['name'] : NULL,
  ];

  $form['secondary'] = [
    '#type' => 'fieldset',
    '#title' => t('Choose One'),
    '#required' => TRUE,
  ];

  $form['secondary']['hankid'] = [
    '#type' => 'textfield',
    '#title' => t('Student ID Number'),
    '#size' => 10,
    '#maxlength' => 10,
    '#element_validate' => ['element_validate_number'],
    '#default_value' => !empty($form_state['values']['hankid']) ? $form_state['values']['hankid'] : NULL,
  ];

  $form['secondary']['ssn'] = [
    '#type' => 'textfield', // password
    '#title' => t('SSN (Last four digits)'),
    '#size' => 4,
    '#maxlength' => 4,
    '#element_validate' => ['element_validate_number'],
    '#default_value' => !empty($form_state['values']['ssn']) ? $form_state['values']['ssn'] : NULL,
  ];

  $form['secondary']['birthdate'] = [
    '#type' => 'date_popup',
    '#title' => t('Birth Date'),
    '#date_format' => 'm/d/Y',
    '#date_label_position' => 'none',
    '#date_year_range' => '-100:0',
  ];

  $form['submit'] = [
    '#type' => 'submit',
    '#value' => t('Search'),
  ];

  if (!empty($form_state['values']['results'])) {
    $form['results'] = [
      '#prefix' => '<div class="loginsearch-results">',
      $form_state['values']['results'],
      '#suffix' => '</div>',
    ];
  }

  return $form;
}

/**
 * Validate search form entry.
 */
function loginsearch_lookup_form_validate($form, &$form_state) {
  $values = $form_state['values'];
  if (empty($values['hankid']) && empty($values['ssn']) && empty($values['birthdate'])) {
    form_set_error('secondary', t('You must provide one of either Student ID Number, SSN, or Birth Date'));
  }
}

/**
 * Process Login Search form submissions.
 */
function loginsearch_lookup_form_submit($form, &$form_state) {
  $options = ['name' => $form_state['values']['name']];
  foreach (['hankid', 'ssn', 'birthdate'] as $key) {
    $value = trim($form_state['values'][$key]);
    if (!empty($value)) {
      $options[$key] = $value;
    }
  }
  $form_state['values']['results'] = LoginSearchHank::create($options)->searchResults();
  $form_state["rebuild"] = TRUE;
}
