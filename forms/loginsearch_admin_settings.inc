<?php

/**
 * @file
 * This file contains admin forms for the loginsearch module.
 *
 * @see loginsearch.module
 */

/**
 * Creates an administrative form for loginsearch settings.
 */
function loginsearch_admin_settings() {
  $form = [];

  $ldap_servers = ldap_servers_get_servers(NULL, 'all');
  $options = [];
  foreach ($ldap_servers as $key => $item) {
    $options[$key] = $item->name;
  }

  $form['loginsearch_ldap_server'] = [
    '#title' => t('LDAP Server'),
    '#type' => 'select',
    '#options' => $options,
    '#description' => t('Select LDAP connection to use for Login Search.'),
    '#default_value' => variable_get('loginsearch_ldap_server', NULL),
    '#required' => TRUE,
  ];

  $form['loginsearch_password_help'] = [
    '#title' => t('Password Help Link'),
    '#type' => 'textfield',
    '#size' => 80,
    '#description' => t('Enter the link target for the password help page.'),
    '#default_value' => variable_get('loginsearch_password_help', HFC_PWHELP),
    '#required' => TRUE,
  ];

  $form['loginsearch_timeout'] = [
    '#title' => t('HANK API Timeout'),
    '#type' => 'textfield',
    '#size' => 3,
    '#description' => t('Set a timeout in seconds for the HANK API. Recommended to not exceed 30 seconds.'),
    '#element_validate' => ['element_validate_integer'],
    '#default_value' => variable_get('loginsearch_timeout', 5),
  ];

  return system_settings_form($form);
}
