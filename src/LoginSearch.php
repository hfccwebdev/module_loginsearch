<?php

/**
 * Defines the Login Search class.
 */
class LoginSearch {

  /**
   * Default link to Self-service change password.
   */
  const HFC_PWCHANGE = 'https://sspr.hfcc.edu/sspr/private/changepassword';

  /**
   * Stores flag to determine if lookup has been performed.
   */
  private $processed;

  /**
   * Stores ldapQuery results.
   */
  private $matches;

  /**
   * Stores supplied options for later display.
   */
  private $options;

  /**
   * Instantiates a new object of this class.
   */
  public static function create($options = []) {
    return new static($options);
  }

  /**
   * Construct new object.
   */
  public function __construct($options = []) {

    if (!empty($options['name'])) {

      $name = check_plain($options['name']);
      if (is_numeric($name)) {
        $options['name'] = substr('0000000'. $name, -7);
      }
    }

    if (!empty($options['hankid'])) {
      $hankid = check_plain($options['hankid']);
      if (is_numeric($hankid)) {
        $options['hankid'] = substr('0000000'. $hankid, -7);
      }
    }

    if (!empty($options['hanktools'])) {
      $options['hankid'] = $options['hanktools']->formatId();
      $options['name'] = $options['hanktools']->username();
      $options['single'] = TRUE;
    }

    // @todo: This is a badly named flag for non-form-based lookups.
    $options['single'] = !empty($options['single']) ? TRUE : FALSE;

    $this->options = $options;

    // @todo: Probably never do a lookup until it's time to do a lookup.
    // This would require a flag set somewhere to know if we've tried or not.
    // (Because results can be empty.)
    // Adding redundant code in userLookup() for now.
    if (isset($options['name']) or isset($options['hankid'])) {
      $this->userLookup();
    }
  }

  /**
   * Get Current User Info.
   *
   * Verifies that the current user is ldap-authenticated.
   */
  private function getCurrentUser() {

    global $user;
    $options = [];

    if ($user->uid > 1) {
      if ($this->checkAuthMap($user->uid, $user->name)) {
        $options['name'] = $user->name;
        if (!empty($user->field_user_hank_id[LANGUAGE_NONE][0]['value'])) {
          $options['hankid'] = $user->field_user_hank_id[LANGUAGE_NONE][0]['value'];
        }
        $options['single'] = TRUE;
      }
    }
    return $options;
  }

  /**
   * Verify authmap indicates ldap user.
   *
   * @param $uid
   *   Drupal user uid.
   *
   * @param $name
   *   Drupal user name.
   */
  private function checkAuthMap($uid, $name) {
    $query = "SELECT * FROM {authmap} WHERE (uid = :uid OR authname = :name) AND module = 'ldap_user'";
    $params = [':uid' => $uid, ':name' => $name];
    if (!empty(db_query($query, $params)->fetchAll())) {
      return TRUE;
    }
  }

  /**
   * Queries LDAP server for the user.
   *
   * This was adapted from _ldapauth_user_lookup() in ldapauth.module.
   *
   * @param $name
   *   A login name.
   *
   * @return
   *   An array with matching user data or NULL if not found.
   */
  private function userLookup() {

    if (isset($this->options['name']) or isset($this->options['hankid'])) {
      $options = $this->options;
    }
    else {
      $options = $this->getCurrentUser();
    }

    if (empty($options['name'])) {
      // Exit if we can't find a username.
      // Most likely cases are Anonymous or admin.
      return;
    }

    if (!empty($options['single']) && $options['single']) {
      $filter = "(|(cn={$options['name']})(workforceID={$options['name']}))";
    }
    else {
      $filters = ["(sn={$options['name']})", "(!(hfccPrivacyFlag=*))"];
      if (!empty($options['hankid'])) {
        $filters[] = "(workforceID={$options['hankid']})";
      }
      if (!empty($options['ssn'])) {
        $filters[] = "(hfcclast4ssn={$options['ssn']})";
      }
      if (!empty($options['birthdate'])) {
        $birthdate = preg_replace('|^....-(..)-(..)$|', '\\1\\2', $options['birthdate']);
        $filters[] = "(hfccbirthday={$birthdate})";
      }
      $filter = "(&" . implode('', $filters) . ")";
    }

    if ($sid = variable_get('loginsearch_ldap_server', NULL)) {
      $result = $this->ldapQuery($sid, $filter);

      if ($result) {
        $rows = [];
        foreach ($result as $entry) {
          if (is_array($entry)) {
            $rows[] = $this->userLookupResultsRow($entry);
          }
        }
        $this->matches = $rows;
        // @todo: Remove after everything else is rewritten to use $this->matches instead.
        return $rows;
      }
    }
    else {
      drupal_set_message(t('LDAP server not set. Please configure login search.'));
    }
  }

  /**
   * Execute LDAP query.
   *
   * @see ldap_servers_test_form_submit()
   * @see getpersoninfo_ldap_query()
   */
  private function ldapQuery($sid, $filter) {

    $ldap_server = ldap_servers_get_servers($sid, 'all', TRUE);

    $ldap_server->connect();
    $ldap_server->bind();

    $results = [];

    $count = 0;

    foreach ($ldap_server->basedn as $base_dn) {
      $result = $ldap_server->search($base_dn, $filter, [], 0, 0, 90, NULL, LDAP_SCOPE_SUBTREE);
      if ($result !== FALSE && $result['count'] > 0) {
        $count = $count + $result['count'];
        $results = array_merge($results, $result);
      }
    }

    $entries = [];
    for ($entry=0; $entry < $count; $entry++) {
      $items = ['dn' => $results[$entry]['dn']];
      foreach ($results[$entry] as $key => $value) {
        if (!is_numeric($key) && !empty($value['count']) && $value['count'] > 0) {
          for ($i=0; $i < $value['count']; $i++) {
            $items[$key][$i] = check_plain(mb_convert_encoding($value[$i], 'UTF-8'));
          }
        }
      }
      $entries[$entry] = $items;
    }
    return $entries;
  }

  /**
   * Get the matched user.
   */
  public function getMatchedUser() {
    if (!$this->processed) {
      $this->userLookup();
      $this->processed = TRUE;
    }
    return !empty($this->matches) ? reset($this->matches) : FALSE;
  }

  /**
   * Verifies a single match was found.
   */
  public function found() {
    return (!empty($this->options['single']) && !empty($this->matches[0]));
  }

  /**
   * Generate Search Results output.
   */
  public function searchResults() {

    if ($match = $this->getMatchedUser()) {
      $rows = [];
      if (!empty($match['fullname'])) {
        $rows[] = $match['fullname'] . ' (' . $match['cn'] . ')';
      }
      sort($rows);
      $title = t('Match found:');
      $results = [
        '#theme' => 'item_list',
        '#items' => $rows,
        '#title' => $title,
      ];
    }
    else {
      $results = [
        '#prefix' => '<span class="not-found">',
        '#markup' => t('No match found.'),
        '#suffix' => '</span>',
      ];
    }
    return $results;
  }

  /**
   * Build an entry for the userLookup results.
   */
  private function userLookupResultsRow($entry) {
    $row = [];
    $row['dn'] = $entry['dn'];
    $row['cn'] = $entry['cn'][0];
    if (!empty($entry['fullname'][0])) {
      $row['fullname'] = $entry['fullname'][0];
    }
    else {
      watchdog('loginsearch', 'Missing fullname property for user %dn', ['%dn' => $entry['dn']], WATCHDOG_WARNING);
      $row['fullname'] = t('Name value missing');
    }
    $row['logintime'] = !empty($entry['logintime'][0]) ? $entry['logintime'][0] : NULL;
    $row['passwordexpirationtime'] = !empty($entry['passwordexpirationtime'][0]) ? $entry['passwordexpirationtime'][0] : NULL;
    $row['logingraceremaining'] = !empty($entry['logingraceremaining'][0]) ? $entry['logingraceremaining'][0] : NULL;
    $row['logingracelimit'] = !empty($entry['logingracelimit'][0]) ? $entry['logingracelimit'][0] : NULL;
    $row['logindisabled'] = !empty($entry['logindisabled'][0]) ? $entry['logindisabled'][0] : 'FALSE';
    return $row;
  }

  /**
   * Get attribute from single search result.
   */
  public function getAttribute($name) {
    if (!empty($this->options['single']) && !empty($this->matches[0])) {
      return !empty($this->matches[0][$name]) ? $this->matches[0][$name] : NULL;
    }
  }

  /**
   * Returns the last login time.
   */
  public function loginTime() {
    return strtotime($this->getAttribute('logintime'));
  }

  /**
   * Returns the password expiration date.
   */
  public function passwordExpirationTime() {
    return strtotime($this->getAttribute('passwordexpirationtime'));
  }

  /**
   * Returns the number of remaining grace logins.
   */
  public function loginGraceRemaining() {
    return $this->getAttribute('logingraceremaining');
  }

  /**
   * Check the current user's HFC password expiration and annoy them if it's about to expire.
   */
  public function expirationWarning() {
    global $user;
    if ($user->uid > 1) {
      $cid = 'loginsearch_expire_message_' . $user->uid;
      if ($cache = cache_get($cid)) {
        $expires = $cache->data;
      }
      else {
        $expires = 0;
      }
      if (REQUEST_TIME > $expires) {
        if ($matches = $this->userLookup()) {
          $match = reset($matches);
          if (!empty($match['passwordexpirationtime'])) {
            $delta = round((strtotime($match['passwordexpirationtime']) - REQUEST_TIME)/86400);
            $grace = !empty($match['logingraceremaining']) ? $match['logingraceremaining'] : 0;
            $values = ['%d' => $delta, '%g' => $grace, '!p' => self::HFC_PWCHANGE];
            if ($delta < 1) {
              drupal_set_message(t('Your password has expired You have %g grace logins remaining. <a href="!p">Change your password now!</a>', $values), 'error');
              $sleep = 0;
            }
            elseif ($delta < 15) {
              drupal_set_message(t('Your password expires in %d days. You should <a href="!p">change your password soon!</a>', $values), 'warning');
              $sleep = ($delta < 5) ? ($delta - 1) * 30 : $delta * 60;
            }
            else {
              $sleep = 86400;
            }
          }
          else {
            $sleep = 86400;
          }
          $expires = REQUEST_TIME + $sleep;
          cache_set($cid, $expires, 'cache');
        }
      }
    }
  }

  /**
   * Generate Expiration Check Results.
   */
  public function expirationResults() {
    if (!empty($this->matches)) {
      $rows = [];
      foreach ($this->matches as $match) {
        $cn = $match['cn'];
        $fullname = $match['fullname'];
        $values = [
          '%yourname' => $fullname . ' (' . $cn . ')',
          '@expiredate' => !empty($match['passwordexpirationtime']) ? $this->novellDateFormat($match['passwordexpirationtime']) : NULL,
          '%grace' => $match['logingraceremaining'],
          '!pwchange' => self::HFC_PWCHANGE,
        ];
        if (!empty($match['passwordexpirationtime'])) {
          if ($this->novellDateCheck($match['passwordexpirationtime'])) {
            if (intval($match['logingraceremaining']) > 0) {
              $message = t('%yourname: password expired on @expiredate! You have %grace grace logins remaining. <a href="!pwchange">Change your password now!</a>', $values);
            }
            else {
              $message = t('%yourname: password expired on @expiredate! You have %grace grace logins remaining. Please call the Help Desk for assistance.', $values);
            }
            $class = 'loginsearch-expired';
          }
          else {
            $message = t('%yourname: password expires on @expiredate.', $values);
            $class = 'loginsearch-not-expired';
          }
        }
        else {
          $message = t('%yourname: password never expires.', $values);
          $class = 'loginsearch-not-expired';
        }
        $rows[] = ['data' => $message, 'class' => [$class]];
      }
      sort($rows);
      $title = t('Matches found for @name:', ['@name' => $this->options['name']]);
      $results = ['#theme' => 'item_list', '#items' => $rows, '#title' => $title];
    }
    else {
      $results = [
        '#prefix' => '<span class="not-found">',
        '#markup' => t('No match found for @name.', ['@name' => $this->options['name']]),
        '#suffix' => '</span>',
      ];
    }
    return $results;
  }

  /**
   * Check expiration date.
   */
  public function novellDateCheck($datevalue) {
    $nowdate = gmdate('YmdHis') ."Z";
    if ($nowdate > $datevalue) {
      $output = TRUE;
    }
    else {
      $output = FALSE;
    }
    return $output;
  }

  /**
   * Make pretty date.
   */
  public function novellDateFormat($datevalue) {
    if($pos = strpos($datevalue, '.')) {
      $datevalue = substr($datevalue, 0, $pos) . 'Z';
    }
    return date('F j, Y g:ia', strtotime($datevalue));
  }
}
