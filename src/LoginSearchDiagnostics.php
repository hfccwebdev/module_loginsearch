<?php

/**
 * Defines the LoginSearchDiagnostics class.
 */
class LoginSearchDiagnostics {

  /**
   * Implements hook_mysite_user_diagnotics_alter().
   */
  public static function alter(&$output, &$account) {
    if (!empty($account->field_user_hank_id)) {
      $hank_id = $account->field_user_hank_id[LANGUAGE_NONE][0]['safe_value'];
      $hanktools = HankTools::create($hank_id);
      self::showAlerts($output, $hanktools);
    }
  }

  /**
   * Add the alerts content to the output array.
   */
  public static function showAlerts(&$output, &$hanktools) {
    $ldapinfo = LoginSearch::create(['hanktools' => $hanktools]);

    if ($ldapinfo->found()) {

      $output['myhank_support_summary']['summary']['#items'][] = [
        'data' => t('<strong>Last Login Time:</strong> @t', ['@t' => !empty($ldapinfo->loginTime()) ? self::formatDate($ldapinfo->loginTime()) : 'never']),
      ];

      if ($expires = $ldapinfo->passwordExpirationTime()) {
        $expiration = self::formatDate($expires);
        $class = (REQUEST_TIME > $expires) ? 'loginsearch-expired' : 'loginsearch-not-expired';
      }
      else {
        $expiration = 'never';
        $class = 'loginsearch-not-expired';
      }
      $output['myhank_support_summary']['summary']['#items'][] = [
        'data' => t('<strong>Password Expires:</strong> @t', ['@t' => $expiration]),
        'class' => [$class],
      ];

      if ($grace = $ldapinfo->loginGraceRemaining()) {
        $class = (intval($grace) < 5) ? 'loginsearch-expired' : 'loginsearch-not-expired';
        $output['myhank_support_summary']['summary']['#items'][] = [
          'data' => t('<strong>Grace Logins Remaining:</strong> @t', ['@t' => $grace]),
          'class' => [$class],
        ];
      }

      $disabled = $ldapinfo->getAttribute('logindisabled');
      $class = ($disabled == 'TRUE') ? 'loginsearch-expired' : 'loginsearch-not-expired';
      $output['myhank_support_summary']['summary']['#items'][] = [
        'data' => t('<strong>Login Disabled:</strong> @t', ['@t' => $disabled]),
        'class' => [$class],
      ];
    }
    else {
      $output['myhank_support_summary']['summary']['#items'][] = [
        'data' => t('Password status information not found. Please try again.'),
        'class' => ['not-found'],
      ];
    }
  }

  /**
   * Manage data formatting.
   */
  private static function formatDate($date) {
    return format_date($date, 'custom', 'F j, Y g:ia');
  }
}
