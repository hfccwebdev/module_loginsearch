<?php

/**
 * Defines the Login Search page.
 */
class LoginSearchPage {

  /**
   * Defines the default path to the password help page.
   */
  const HFC_PWHELP = 'https://www.hfcc.edu/password';

  /**
   * Page callback for Login Search.
   */
  public static function view() {

    $output = [];

    module_load_include('inc', 'loginsearch', 'forms/loginsearch_lookup_form');

    $output[] = [
      '#prefix' => '<p>',
      '#markup' => t('
        This form can be used to search your HFC universal login information.
        You must fill in your last name, and either your Student ID Number, the last four digits of your Social Security Number, or your birthdate.
      '),
      '#suffix' => '</p>',
    ];
    $output[] = drupal_get_form('loginsearch_lookup_form');
    $output[] = [
      '#prefix' => '<p>',
      '#markup' => t('See <a href="!pwlink">!pwlink</a> for more information about logins and passwords.',
        ['!pwlink' => variable_get('loginsearch_password_help', self::HFC_PWHELP)]
      ),
      '#suffix' => '</p>',
    ];
    return $output;
  }
}
