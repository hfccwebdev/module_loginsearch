<?php

/**
 * The LoginSearchHank service.
 *
 * Implements new login search using HANK API.
 *
 * @see https://dvc.hfcc.net/webadmin/issues/issue7411
 */
class LoginSearchHank {

  /**
   * Stores supplied options for later use.
   */
  private $options;

  /**
   * Instantiates a new object of this class.
   */
  public static function create($options = []) {
    return new static($options);
  }

  /**
   * Construct new object.
   */
  public function __construct($options = []) {

    if (!empty($options['name'])) {
      $options['name'] = filter_xss($options['name'],[]);
    }

    if (!empty($options['hankid'])) {
      $hankid = filter_xss($options['hankid'],[]);
      if (is_numeric($hankid)) {
        $options['hankid'] = str_pad($hankid, 7, "0", STR_PAD_LEFT);
      }
    }

    $this->options = $options;
  }

  /**
   * Search HANK API by lastname.
   */
  public function searchLastName() {
    if (!empty($this->options['name'])) {
      $timeout = variable_get('loginsearch_timeout', 5);
      return HfcHankApi::getData('getpersoninfo-lastname', $this->options['name'], [], TRUE, NULL, $timeout);
    }
    else {
      return [];
    }
  }

  /**
   * Get HANK Identity info from API.
   */
  public function getHankIdentity($hankid) {
    $info = HfcHankApi::getData('hank-identity-info', $hankid);
    return !empty($info) ? reset($info) : NULL;
  }

  /**
   * Generate Search Results output.
   */
  public function searchResults() {

    $rows = [];

    if ($matches = $this->searchLastName()) {
      foreach ($matches as $person) {
        if ($this->checkFilters($person)) {
          $rows[] = $this->getFullName($person);
        }
      }
    }

    if (count($rows) > 1) {
      $results = [
        '#prefix' => '<span class="not-found">',
        '#markup' => t('Could not identify matching user. Please try adding more detail.'),
        '#suffix' => '</span>',
      ];
    }
    elseif (!empty($rows)) {
      sort($rows);
      $results = [
        '#theme' => 'item_list',
        '#title' => t('Match found:'),
        '#items' => $rows,
      ];
    }
    else {
      $results = [
        '#prefix' => '<span class="not-found">',
        '#markup' => t('No match found.'),
        '#suffix' => '</span>',
      ];
    }
    return $results;
  }

  /**
   * Check person against provided option filters.
   *
   * @return bool
   *   Returns TRUE if person matches all provided filters.
   */
  private function checkFilters($person) {
    if (isset($this->options['hankid']) && ($person->ID !== $this->options['hankid'])) {
      return FALSE;
    }
    if (isset($this->options['ssn']) && ($person->LAST4SSN !== $this->options['ssn'])) {
      return FALSE;
    }
    if (isset($this->options['birthdate'])) {
      // Pad BIRTH_DATE_UX by 4 hours because DST and timezone problems.
      // @see https://dvc.hfcc.net/hank/issues/issue5705
      if (empty($person->BIRTH_DATE_UX) || format_date($person->BIRTH_DATE_UX + 14400, 'custom', 'Y-m-d') !== $this->options['birthdate']) {
        return FALSE;
      }
    }
    return TRUE;
  }

  /**
   * Assemble full name from returned parts.
   */
  private function getFullName($person) {
    $hank_identity = $this->getHankIdentity($person->ID);
    $names = [];

    if (!empty($hank_identity->H19_IDV_PREFERRED_NAME)) {
      $names[] = $hank_identity->H19_IDV_PREFERRED_NAME;
    }
    elseif (!empty($hank_identity->H19_IDV_FIRST_NAME) || !empty($hank_identity->H19_IDV_LAST_NAME)) {
      foreach (['H19_IDV_FIRST_NAME', 'H19_IDV_MIDDLE_INIT', 'H19_IDV_LAST_NAME', 'H19_IDV_SUFFIX'] as $fieldname) {
        if (!empty($hank_identity->{$fieldname})) {
          $names[] = $hank_identity->{$fieldname};
        }
      }
    }
    elseif (!empty($person->FIRST_NAME) || !empty($person->LAST_NAME)) {
      foreach (['FIRST_NAME', 'MIDDLE_INIT', 'LAST_NAME'] as $fieldname) {
        if (!empty($person->$fieldname)) {
          $names[] = $person->$fieldname;
        }
      }
    }

    if (!empty($hank_identity->H19_IDV_OEE_USERNAME)) {
      $names[] = '(' . $hank_identity->H19_IDV_OEE_USERNAME . ')';
    }
    else {
      $names[] = '<em>(No username found. Please contact Help Desk.)</em>';
    }

    return implode(' ', $names);
  }
}
